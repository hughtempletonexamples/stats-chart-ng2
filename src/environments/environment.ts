// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyA8bCy6-Ni8ZEBK9GhZ3htmJFXh10FPka4",
    authDomain: "stat-chart.firebaseapp.com",
    databaseURL: "https://stat-chart.firebaseio.com",
    projectId: "stat-chart",
    storageBucket: "stat-chart.appspot.com",
    messagingSenderId: "248549607778"
  }
};
