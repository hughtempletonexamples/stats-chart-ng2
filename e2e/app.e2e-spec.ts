import { StatsChartNg2Page } from './app.po';

describe('stats-chart-ng2 App', () => {
  let page: StatsChartNg2Page;

  beforeEach(() => {
    page = new StatsChartNg2Page();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
